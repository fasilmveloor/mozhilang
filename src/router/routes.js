const routes = [
  {
    path: "/",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "", component: () => import("pages/IndexPage.vue") },
      {
        path: "/practice",
        component: () => import("src/pages/PracticePage.vue"),
      },
      {
        path: "/documentation",
        component: () => import("src/pages/DocumentationPage.vue"),
      },
      {
        path: "/examples",
        component: () => import("src/pages/ExamplesPage.vue"),
      },
      {
        path: "/about",
        component: () => import("src/pages/AboutPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
