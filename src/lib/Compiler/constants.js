var KalaamKeywords = {
  Print: "കാണിക്കുക", //Still have to change this manually where REGEX are implemented
  Input: "ഇൻപുട്",
  If: "अगर",
  For: "दुहराओ",
  While: "जबतक",
  Length: "संख्या",
  Push: "पुश",
  Function: "रचना",
  Else: "अन्यथा",
  Langugae: "Hindi",
};

export { KalaamKeywords };
